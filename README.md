# general notes on boosting strategies

# GRADIENT BOOSTING

useful for regression, classification, ranking. 

Uses gradient descent + boosting. 

Fits ensemble model; each iteration a weak learner is added to compensate for errors on high-weighted data points. 

Adaboost most well-known. Adaboost generalized to gradient boosting to handle different loss functions. 

Residuals w = y[i] - f(x[i]) from previous model can be used to improve model predictions. 

L = sum{i} (y[i] - f(x[i]) )^2/2

dL/dw = f(x[i]) - y[i] 

Hence residuals are equivalent to negative gradients, 

y[i] - f(x[i]) = - dL/df(x[i])

We are essentially doing gradient descent on the model. 

# Implementation
We find a function f that is a weighted sum of functions h[i] from a set of weak learners H.

f = sum{i,M} q[i]*h[i](x) + constant

1/ initialize model with a constant 
2/ calculate residuals r[i]
3/ fit base learner fto residuals  (x[i],r[i])
4/ find gamma (weight on jth base learner) by solving 

gamma[j] = arg min {gamma} sum{samples} L(y[i],F[j-1] + gamma*h[j])

5/ New model is now

f[m] = f[m-1] + gamma[m]*h[m]

# Regularization 
Boosted trees can be penalized by using l2, early stopping (number of weak learners added) or removing branches that do not reduce loss beyond a threshold. Minimum number of observations in terminal nodes, tree depth, etc.

Shrinkage
Using a learning rate, 

nu*gamma*h[m], 0 <= nu <= 1

leads to increased generalization but at the cost of computate cycles. 

Stochastic gradient descent
Weak learners can also be fit on a random subsammple of the training set. This helps with regulariation, leads to faster training, and provides a validation-like out-of-bag error. 

# ADAPTIVE BOOSTING (adaboost)
Subsequent weak learners are trained on misclassified data with strengthened weight. Adaboost is sensitive to noisy data and outliers. Evaluating all features reduce training speed, execution, and predictive power. Adaboost selects only features shown to improve predictive power. 

# Training

F[x] = sum{1,T} f[t][x]

error is

E[t] = sum{training data} E( F[t-1] + alpha*h )

# Weighting 
Each iteration during training weight each sample by its E(F[t-1](x[i]))

E[m] = sum{1,N} exp(-y[i]*F[m][x] )

# Derivation
Error of C[m] = sum{i=1,N} exp(-y[i]*C[m](x[i]))

sum of exponential loss on each data point.

want to find weak classifier, k[m],  with lowest weighted error with weights

w[m](i) = exp(-y[i]*C[m-1](x[i]) ) 

can split error between correct and incorrect summations

E = sum{i=1,N} w[m](i)*exp(-alpha[m]) + sum{y[i]!=k[m](x[i])} w[m](i)

only last term contains k[m]. So to min E min last term, ie. lowest weighted error (with weights w[m](i) ).

to find alpha[m] set 

dE/dalpha[m] = 0 

alpha[m] = 1/2 ln( sum{y[i]=k[m](x[i])} w[m](x[i]) / sum{y[i]!=k[m](x[i])} w[m](x[i]) )

where error rate of weak classifier m
epsilon[m] = sum{y[i]!=k[m](x[i])} w[m](x[i]) /  sum{i=1,N} w[m](x[i])

alpha[m] = 1/2 ln( (1 - epsilon[m]) / epsilon[m] )

which is negative logit function * 1/2

Adaboost minimizes total weighted error sum{wrong samples} w[m](x[i])
and calculate error rate, epsilon, which gives alpha[m] and add 
alpha[m]*k[m] to the ensemble

Note that mse excessively weights outliers. 
Exp error fxn givs final E[final] = prod{train data} exp(-y[i]*f(x[i]))
which is product of error of each stage

Other loss fxns: monotonic, differentiable all work.
Boosting can be seen as minimizing a convex loss over a convex set of functions aka gradient descent. Adaboost minimizes exp loss, logitboost minimizes mse or Huber loss.
