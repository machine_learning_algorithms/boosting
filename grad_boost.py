import pandas as pd
from sklearn.model_selection import train_test_split
import lightgbm as lgb 
import matplotlib.pyplot as plt
plt.ion()

# load data 
data = pd.read_csv('clean_data.csv',sep=',') 
# drop columns not used in clustering analysis
drop_columns = ['External Timecard ID','Task Desc','Activity Desc',\
'External Practice Group','External Client Name','External Matter Name',\
'Batch','WorkDate','OpenDate','CloseDate']
data.drop(drop_columns, axis=1, inplace=True)
# drop rows with negative WorkAmt, StdAmt, WorkHrs
data.drop(data[data.WorkAmt < 0].index,inplace=True)
data.drop(data[data.StdAmt < 0].index,inplace=True)
data.drop(data[data.WorkHrs < 0].index,inplace=True)

# convert categories to int type
data.dtypes # data types of each column
float_cols = ['StdAmt','StdRate','WorkAmt','WorkHrs','RefRate','RefAmt'] # columns that are float; not categorical
# convert strings to categorical 
for col in data.columns:
    if col not in float_cols:
        data[col] = data[col].astype('category')
# select categorical columns and convert to int
cat_columns = data.select_dtypes(['category']).columns
data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)

# partition data 60/20/20 train/val/test
# lightGBM can use categorical features as input
train,test = train_test_split(data,test_size=0.2)
train,val = train_test_split(train,test_size=0.25)

# train gradient boosting model
# here we focus on prediction for WorkAmt; but can be changed
lgb_train = lgb.Dataset(train.loc[:,data.columns!='WorkAmt'],train.WorkAmt)
lgb_val = lgb.Dataset(val.loc[:,data.columns!='WorkAmt'],val.WorkAmt,reference=lgb_train)
lgb_test = lgb.Dataset(test.loc[:,test.columns!='WorkAmt'],test.WorkAmt)

# model hyperparameters; elastic net regularizer
params = {
    'boosting_type': 'gbdt', # traditional gradient boosting decision tree
    'objective': 'regression',
    'metric': {'l1','l2'}, # auc, binary_logloss, mse, mae, 
    'num_leaves': 31,
    'learning_rate': 0.05,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5, # resampling every kth iteration
    'verbose': 0
}

# train model 
boost_rounds = 1000
earlystop = 20
evals_result = {} 
gbm = lgb.train(params, 
                lgb_train,
                # init_model=init_gbm,          # enable training from prev. model
                num_boost_round=boost_rounds,   # number of boosting rounds
                early_stopping_rounds=earlystop,# rounds before early stopping
                valid_sets=lgb_val,             # can have more than 1
                evals_result=evals_result,      # store validation results
                # nfold = 10,                   # cross-val folds; use with lgb.cv()
                verbose_eval=True)              # print evaluations 
# save model
gbm.save_model('reedsmith_gbm.mdl', num_iteration=gbm.best_iteration)

# inference
yPred = gbm.predict(test.loc[:,data.columns!='WorkAmt'],num_iteration=gbm.best_iteration)
print('The mean-square error on test is:', ((yPred-test.WorkAmt)**2.0).mean())

# plotting
ax = lgb.plot_metric(evals_result, metric='l1') # metrics
ax = lgb.plot_metric(evals_result, metric='l2') 
ax = lgb.plot_importance(gbm, max_num_features=10) # feature importance
ax = lgb.plot_tree(gbm, tree_index=83, figsize=(20, 8),show_info=['split_gain']) # example tree

