import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import lightgbm as lgb 
import matplotlib.pyplot as plt
plt.ion()

# load data into dataframes
data = pd.read_csv('../data/cleveland_heart.csv')
data.replace('?', np.NaN, inplace=True)

# target is +/- 1 for adaboost
# for cleveland heart dataset convert 0: -1, 1-4: 1
data['num'].replace(0,-1,inplace=True)
data['num'].replace({1:1,2:1,3:1,4:1},inplace=True)

# split data into train / test 
train,test = train_test_split(data,test_size=0.2)
# set initial weights to 1/samples
wInit = 1/train.shape[0]*np.ones([1,train.shape[0]]) # vector of weights on training samples
learnerInit = weaklearner(
earlyStop = 10 # number of rounds or classifiers to add

def expLoss(data,target,classifier):
    # return vector of classification errors
    loss = np.exp( -target*classifier(data)) )
    return loss

def train(data,validSet,target):
    # iterate through weak learners; features x samples
    featureInit = random.sample(list(data.columns),1)
    sampleInit = random.sample(list(data.index),1)
    rowInit = data.iloc[sampleInit] 
    valInit = data[featureInit].median()
    # initialize classifier
    classifier = weakLearner(data,featureInit,valInit) 
    missedRows,predVec,loss = classifer(data)
    ensemble = classifier
    prevValLoss = 0.
    _missedValRows,_predValVec,newValLoss = ensemble(validSet) # TODO
    while prevValLoss-newValLoss > 0:
        for feature in data.columns:
        # unique values in curren column
        uniqueVals = data[feature].sort_values().unique() # ascend
            for value in uniqueVals:
                # construct weak learner 
                classifier = weakLearner(feature,value) 
                # choose h that min weighted sum error for misclassified pts
                weightedError = expLoss(missedRows,target,classifier) # only on misclassified pts
                epsilon = np.dot(weights,loss)/np.sum(weights) # weighted error rate 
                if epsilon < minEpsilon:
                    # update best learner
                    minEpsilon = epsilon
                    bestLearner = classifier 
        # choose alpha
        alpha = learnCoeff(epsilon)
        ensemble += alpha*bestLearner # F[t] = F[t-1] + alpha*h
        # run ensemble on train data
        predVec,loss = ensemble(data)
        # update and renormalize weights
        weights = updateWeights(weights,predVec,target)
        # update validation loss
        prevValLoss = newValLoss 
        _predValVec,newValLoss = ensemble(validSet)
    return classifier

def updateWeights(weights,predVec,target):
    newWeights = weights*np.exp(-target*predVec) # vector on all data points 
    # renormalize new weights sum(weights) = 1
    newWeights = newWeights / np.sum(newWeights)
    return newWeights

def weakLearner(data,feature,value):
    # TODO
    # decision stump
    if sample[feature] < value:
        pred = -1
    else:
        pred = 1
    return missedRows,predVec,loss # vector of predictions for each sample

def learnCoeff(epsilon):
    # when classifier outputs +/- 1
    alpha = 1/2 * np.log((1-epsilon)/epsilon)
    return alpha

